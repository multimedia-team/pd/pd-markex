lib.name = markex
class.sources = \
	abs~.c \
	alternate.c \
	average.c \
	counter.c \
	hsv2rgb.c \
	invert.c \
	multiselect.c \
	oneshot.c \
	randomF.c \
	reson~.c \
	rgb2hsv.c \
	strcat.c \
	tripleLine.c \
	tripleRand.c \
	vector+.c \
	vector-.c \
	vector0x2a.c \
	vector0x2f.c \
	vectorabs.c \
	vectorpack.c \
	$(empty)

lib.setup.sources = \
	markex.c \
	$(empty)

datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(wildcard *.md) \
	$(empty)

datadirs = \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
